package tp_note_master;

public class MasterPleinException extends Exception {
	private static final long serialVersionUID = 1L;
	public MasterPleinException() { }
	public MasterPleinException(String message) { super(message);
	}
}
