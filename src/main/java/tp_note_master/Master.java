package tp_note_master;

import java.util.ArrayList;

public class Master {
	private String code_master;
	private String nom_master;
	private ArrayList<Etudiant> liste_etudiant = new ArrayList<Etudiant>();
	
	
	public String getCode_master() {
		return code_master;
	}
	
	public void setCode_master(String code_master) {
		this.code_master = code_master;
	}
	
	public String getNom_master() {
		return nom_master;
	}
	
	public void setNom_master(String nom_master) {
		this.nom_master = nom_master;
	}
	
	public ArrayList<Etudiant> getListe_etudiant() {
		return liste_etudiant;
	}
	
	public void setListe_etudiant(ArrayList<Etudiant> liste_etudiant) {
		this.liste_etudiant = liste_etudiant;
	}
	
	public boolean masterPlein(ArrayList<Etudiant> liste_etudiant) {
		return (liste_etudiant.size() >= 31); 
	}
	
	
	public boolean etudiantValideMoyenne(ArrayList<Etudiant> liste_etudiant) {
		boolean conforme=true;
		for (int i=0; i<liste_etudiant.size(); i++) {
			if ( liste_etudiant.get(i).getMoyenne()<10 ) {
				conforme=false;
			}
		}
		return conforme;
	}
	
	public boolean etudiantValideDossier(ArrayList<Etudiant> liste_etudiant) {
		boolean conforme=true;
		for (int i=0; i<liste_etudiant.size(); i++) {
			if ( liste_etudiant.get(i).isDossier_complet()==false ) {
				conforme=false;
			}
		}
		return conforme;
	}
	
	public void envoyer() throws MasterPleinException {
        if(!etudiantValideMoyenne(liste_etudiant)) {
            throw new MasterPleinException("L'etudiant n'a pas la moyenne");
        }
        if(!etudiantValideDossier(liste_etudiant)) {
        	throw new MasterPleinException("Le dossier de l'etudiant est incomplet");
        }
        if(!masterPlein(liste_etudiant)) {
        	throw new MasterPleinException("Le Master a deja atteint sa limite d'etudiants");
        }
	}

	
	//Faire des constructeurs avec des exceptions pour la validite de la moyenne et des dossiers
	
	public Master(String code_master,String nom_master,ArrayList<Etudiant> liste_etudiant) throws MasterPleinException{
		this.code_master=code_master;
		this.nom_master=nom_master;
		this.liste_etudiant=liste_etudiant;
	}
	
}
