package tp_note_master;

public class Enseignant {
	private int numero_enseignant;
	private String nom_enseignant;
	private String prenom_enseignant;
	private String code_master;
	
	
	public int getNumero_enseignant() {
		return numero_enseignant;
	}
	
	public void setNumero_enseignant(int numero_enseignant) {
		this.numero_enseignant = numero_enseignant;
	}

	
	public String getNom_enseignant() {
		return nom_enseignant;
	}


	public void setNom_enseignant(String nom_enseignant) {
		this.nom_enseignant = nom_enseignant;
	}


	public String getPrenom_enseignant() {
		return prenom_enseignant;
	}


	public void setPrenom_enseignant(String prenom_enseignant) {
		this.prenom_enseignant = prenom_enseignant;
	}


	public String getCode_master() {
		return code_master;
	}


	public void setCode_master(String code_master) {
		this.code_master = code_master;
	}
	
	public Enseignant(int numero_enseignant, String nom_enseignant, String prenom_enseignant, String code_master) {
		this.numero_enseignant = numero_enseignant;
		this.nom_enseignant = nom_enseignant;
		this.prenom_enseignant = prenom_enseignant;
		this.code_master = code_master;
	}
	
}
