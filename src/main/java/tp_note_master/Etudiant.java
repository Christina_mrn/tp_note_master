package tp_note_master;

public class Etudiant {
	
		private int numero_etudiant;
		private String nom_etudiant;
		private String prenom_etudiant;
		private String licence;
		private double moyenne;
		private boolean dossier_complet;
		
		
		public int getNumero_etudiant() {
			return numero_etudiant;
		}
		
		public void setNumero_etudiant(int numero_etudiant) {
			this.numero_etudiant = numero_etudiant;
		}
		
		public String getNom_etudiant() {
			return nom_etudiant;
		}
		
		public void setNom_etudiant(String nom_etudiant) {
			this.nom_etudiant = nom_etudiant;
		}
		
		public String getPrenom_etudiant() {
			return prenom_etudiant;
		}
		
		public void setPrenom_etudiant(String prenom_etudiant) {
			this.prenom_etudiant = prenom_etudiant;
		}
		
		public String getLicence() {
			return licence;
		}
		
		public void setLicence(String licence) {
			this.licence = licence;
		}
		
		public double getMoyenne() {
			return moyenne;
		}
		
		public void setMoyenne(double moyenne) {
			this.moyenne = moyenne;
		}

		public boolean isDossier_complet() {
			return dossier_complet;
		}

		public void setDossier_complet(boolean dossier_complet) {
			this.dossier_complet = dossier_complet;
		}
		
		public Etudiant(int numero_etudiant,String nom_etudiant,String prenom_etudiant,String licence,double moyenne,boolean dossier_complet) {
			this.numero_etudiant=numero_etudiant;
			this.nom_etudiant=nom_etudiant;
			this.prenom_etudiant=prenom_etudiant;
			this.moyenne=moyenne;
			this.licence=licence;
			this.dossier_complet=dossier_complet;
		}
	
			
}
