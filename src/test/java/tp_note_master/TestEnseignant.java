package tp_note_master;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class EnseignantTest {
	
	private Enseignant enseignant_GL;
	private Enseignant enseignant_IASD;
	private Enseignant enseignant_IMAGINE;
	private Enseignant enseignant_ALGO;
	private Enseignant enseignant_BIOINFO;
	
		
	@BeforeEach
	public void init( ) {
		enseignant_GL = new Enseignant(12345678,"Seriai","Djamel","MAGL");
		enseignant_IASD = new Enseignant(23456789,"Retore","Christian","MAIASD");
		enseignant_IMAGINE = new Enseignant(19876543,"Puech","William","MAIMAGINE");
		enseignant_ALGO = new Enseignant(45612389,"Durand","Bruno","MAALGO");
		enseignant_BIOINFO = new Enseignant(58469321,"Chateau","Annie","MABIOINFO");
	}
	

	@Test
	void testNumeroEnseignant() {
		enseignant_GL.setNumero_enseignant(23165465);
		assertEquals(23165465,enseignant_GL.getNumero_enseignant());
		enseignant_IASD.setNumero_enseignant(23451321);
		assertEquals(23451321,enseignant_IASD.getNumero_enseignant());
		enseignant_IMAGINE.setNumero_enseignant(19856566);
		assertEquals(19856566,enseignant_IMAGINE.getNumero_enseignant());
		enseignant_ALGO.setNumero_enseignant(45532134);
		assertEquals(45532134,enseignant_ALGO.getNumero_enseignant());
		enseignant_BIOINFO.setNumero_enseignant(58321546);
		assertEquals(58321546,enseignant_BIOINFO.getNumero_enseignant());
	}
	
	@Test
	void testNomEnseignant() {
		enseignant_GL.setNom_enseignant("Seri");
		assertEquals("Seri",enseignant_GL.getNom_enseignant());
		enseignant_IASD.setNom_enseignant("Retor");
		assertEquals("Retor",enseignant_IASD.getNom_enseignant());
		enseignant_IMAGINE.setNom_enseignant("Puec");
		assertEquals("Puec",enseignant_IMAGINE.getNom_enseignant());
		enseignant_ALGO.setNom_enseignant("Dura");
		assertEquals("Dura",enseignant_ALGO.getNom_enseignant());
		enseignant_BIOINFO.setNom_enseignant("Chat");
		assertEquals("Chat",enseignant_BIOINFO.getNom_enseignant());
	}
	
	@Test
	void testPrenomEnseignant() {
		enseignant_GL.setPrenom_enseignant("Dja");
		assertEquals("Dja",enseignant_GL.getPrenom_enseignant());
		enseignant_IASD.setPrenom_enseignant("Chris");
		assertEquals("Chris",enseignant_IASD.getPrenom_enseignant());
		enseignant_IMAGINE.setPrenom_enseignant("Will");
		assertEquals("Will",enseignant_IMAGINE.getPrenom_enseignant());
		enseignant_ALGO.setPrenom_enseignant("Bruce");
		assertEquals("Bruce",enseignant_ALGO.getPrenom_enseignant());
		enseignant_BIOINFO.setPrenom_enseignant("Anne");
		assertEquals("Anne",enseignant_BIOINFO.getPrenom_enseignant());
	}
	
	@Test
	void testCodeMaster() {
		enseignant_GL.setCode_master("MAGL1");
		assertEquals("MAGL1",enseignant_GL.getCode_master());
		enseignant_IASD.setCode_master("MAIASD1");
		assertEquals("MAIASD1",enseignant_IASD.getCode_master());
		enseignant_IMAGINE.setCode_master("MAIMAGINE1");
		assertEquals("MAIMAGINE1",enseignant_IMAGINE.getCode_master());
		enseignant_ALGO.setCode_master("MAALGO1");
		assertEquals("MAALGO1",enseignant_ALGO.getCode_master());
		enseignant_BIOINFO.setCode_master("MABIOINFO1");
		assertEquals("MABIOINFO1",enseignant_BIOINFO.getCode_master());
	}
	
	
	


}
