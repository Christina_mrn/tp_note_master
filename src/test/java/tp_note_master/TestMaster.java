package tp_note_master;

import static org.junit.jupiter.api.Assertions.*;

import java.util.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestMaster {

	private Master master_GL;
	private Master master_IASD;
	private Master master_IMAGINE;
	private Master master_ALGO;
	private Master master_BIOINFO;
	
	Etudiant etudiant1 = new Etudiant(21811904,"Chassain","Julie","Informatique",15.48,true);
	Etudiant etudiant2 = new Etudiant(21811903,"Gado","Bryan","Informatique",14.65,true);
	Etudiant etudiant3 = new Etudiant(21811905,"Maurin","Christina","Informatique",11.54,true);
	Etudiant etudiant4 = new Etudiant(21811978,"Sorreau","Lucie","Informatique",10.21,true);
	Etudiant etudiant5 = new Etudiant(21886137,"Claude","Jean","Informatique",13.45,true);
	Etudiant etudiant6 = new Etudiant(21854787,"Tapis","Bernard","Informatique",7.58,true);
	Etudiant etudiant7 = new Etudiant(21856931,"Segara","Helene","Informatique",12.34,false);
	
	
	@BeforeEach
	void init() throws MasterPleinException {
		ArrayList<Etudiant> liste_etudiant_GL = new ArrayList<Etudiant>();
		liste_etudiant_GL.add(etudiant1);
		liste_etudiant_GL.add(etudiant2);
		liste_etudiant_GL.add(etudiant5);
		master_GL = new Master("MAGL","Master GL",liste_etudiant_GL);
		ArrayList<Etudiant> liste_etudiant_IASD = new ArrayList<Etudiant>();
		liste_etudiant_IASD.add(etudiant7);
		master_IASD = new Master("MAIASD","Master IASD",liste_etudiant_IASD);
		ArrayList<Etudiant> liste_etudiant_IMAGINE = new ArrayList<Etudiant>();
		liste_etudiant_IMAGINE.add(etudiant6);
		master_IMAGINE = new Master("MAIMAGINE","Master IMAGINE",liste_etudiant_IMAGINE);
		ArrayList<Etudiant> liste_etudiant_ALGO = new ArrayList<Etudiant>();
		liste_etudiant_ALGO.add(etudiant1);
		liste_etudiant_ALGO.add(etudiant2);
		liste_etudiant_ALGO.add(etudiant3);
		liste_etudiant_ALGO.add(etudiant4);
		liste_etudiant_ALGO.add(etudiant5);
		master_ALGO = new Master("MAALGO","Master ALGO",liste_etudiant_ALGO);
		ArrayList<Etudiant> liste_etudiant_BIOINFO = new ArrayList<Etudiant>();
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant1);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant5);
		liste_etudiant_BIOINFO.add(etudiant2);
		liste_etudiant_BIOINFO.add(etudiant2);
		master_BIOINFO = new Master("MABIOINFO","Master BIOINFO",liste_etudiant_BIOINFO);
	}
	
	@Test
	void testGetCodeMaster() {
		master_GL.setCode_master("MAGL1");
		assertEquals("MAGL1",master_GL.getCode_master());
		master_IASD.setCode_master("MAIASD1");
		assertEquals("MAIASD1",master_IASD.getCode_master());
		master_IMAGINE.setCode_master("MAIMAGINE1");
		assertEquals("MAIMAGINE1",master_IMAGINE.getCode_master());
		master_ALGO.setCode_master("MAALGO1");
		assertEquals("MAALGO1",master_ALGO.getCode_master());
		master_BIOINFO.setCode_master("MABIOINFO1");
		assertEquals("MABIOINFO1",master_BIOINFO.getCode_master());
	}
	
	@Test
	void testGetNomMaster() {
		master_GL.setNom_master("Master Genie Logiciel");
		assertEquals("Master Genie Logiciel",master_GL.getNom_master());
		master_IASD.setNom_master("Master Intelligence Artificielle et sciences des donnees");
		assertEquals("Master Intelligence Artificielle et sciences des donnees",master_IASD.getNom_master());
		master_IMAGINE.setNom_master("Master JV");
		assertEquals("Master JV",master_IMAGINE.getNom_master());
		master_ALGO.setNom_master("Master Algorithme");
		assertEquals("Master Algorithme",master_ALGO.getNom_master());
		master_BIOINFO.setNom_master("Master Bio-Informatique");
		assertEquals("Master Bio-Informatique",master_BIOINFO.getNom_master());
	}
	
	@Test
	void testGetListeEtudiantMaster() {
		ArrayList<Etudiant> testListeEtu = new ArrayList<Etudiant>();
		testListeEtu.add(etudiant6);		
		assertArrayEquals(testListeEtu.toArray(),master_IMAGINE.getListe_etudiant().toArray());
	}
	
	@Test
	void testMasterPlein() {
		MasterPleinException thrown = assertThrows(MasterPleinException.class, () -> {master_BIOINFO.envoyer();});
        assertTrue(thrown.getMessage().equals("Le Master a deja atteint sa limite d'etudiants"));
	};
	
	@Test
	void testDossierIncomplet() {
		MasterPleinException thrown = assertThrows(MasterPleinException.class, () -> {master_IASD.envoyer();});
		assertTrue(thrown.getMessage().equals("Le dossier de l'etudiant est incomplet"));
	}
	
	@Test
	void testMoyenneIncorrect() {
		MasterPleinException thrown = assertThrows(MasterPleinException.class, () -> {master_IMAGINE.envoyer();});
		assertTrue(thrown.getMessage().equals("L'etudiant n'a pas la moyenne"));
	}	
}

	