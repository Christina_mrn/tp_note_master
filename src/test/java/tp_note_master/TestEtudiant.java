package tp_note_master;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TestEtudiant {
	
	private Etudiant etudiant1;
	private Etudiant etudiant2;
	private Etudiant etudiant3;
	private Etudiant etudiant4;
	private Etudiant etudiant5;
	private Etudiant etudiant6;
	private Etudiant etudiant7;
	
	@BeforeEach
	public void init() {
		etudiant1 = new Etudiant(21811904,"Chassain","Julie","Informatique",15.48,true);
		etudiant2 = new Etudiant(21811903,"Gado","Bryan","Informatique",14.65,true);
		etudiant3 = new Etudiant(21811905,"Maurin","Christina","Informatique",11.54,true);
		etudiant4 = new Etudiant(21811978,"Sorreau","Lucie","Informatique",10.21,true);
		etudiant5 = new Etudiant(21886137,"Claude","Jean","Informatique",13.45,true);
		etudiant6 = new Etudiant(21854787,"Tapis","Bernard","Informatique",17.58,false);
		etudiant7 = new Etudiant(21856931,"Segara","Helene","Informatique",12.34,true);
		
	}
	


	@Test
	void testGetNumeroEtudiant() {
		etudiant1.setNumero_etudiant(21811789);
		assertEquals(21811789,etudiant1.getNumero_etudiant());
		etudiant2.setNumero_etudiant(21811578);
		assertEquals(21811578,etudiant2.getNumero_etudiant());
		etudiant3.setNumero_etudiant(21811123);
		assertEquals(21811123,etudiant3.getNumero_etudiant());
		etudiant4.setNumero_etudiant(21811131);
		assertEquals(21811131,etudiant4.getNumero_etudiant());
		etudiant5.setNumero_etudiant(21886133);
		assertEquals(21886133,etudiant5.getNumero_etudiant());
		etudiant6.setNumero_etudiant(21854456);
		assertEquals(21854456,etudiant6.getNumero_etudiant());
		etudiant7.setNumero_etudiant(21854235);
		assertEquals(21854235,etudiant7.getNumero_etudiant());
	}
	
	@Test
	void testGetNomEtudiant() {
		etudiant1.setNom_etudiant("Chasin");
		assertEquals("Chasin",etudiant1.getNom_etudiant());
		etudiant2.setNom_etudiant("Gad");
		assertEquals("Gad",etudiant2.getNom_etudiant());
		etudiant3.setNom_etudiant("Marin");
		assertEquals("Marin",etudiant3.getNom_etudiant());
		etudiant4.setNom_etudiant("Sorau");
		assertEquals("Sorau",etudiant4.getNom_etudiant());
		etudiant5.setNom_etudiant("Caude");
		assertEquals("Caude",etudiant5.getNom_etudiant());
		etudiant6.setNom_etudiant("Tai");
		assertEquals("Tai",etudiant6.getNom_etudiant());
		etudiant7.setNom_etudiant("Seg");
		assertEquals("Seg",etudiant7.getNom_etudiant());
	}
	
	@Test
	void testGetPrenomEtduiant() {
		etudiant1.setPrenom_etudiant("Julia");
		assertEquals("Julia",etudiant1.getPrenom_etudiant());
		etudiant2.setPrenom_etudiant("Bryana");
		assertEquals("Bryana",etudiant2.getPrenom_etudiant());
		etudiant3.setPrenom_etudiant("Christine");
		assertEquals("Christine",etudiant3.getPrenom_etudiant());
		etudiant4.setPrenom_etudiant("Lucille");
		assertEquals("Lucille",etudiant4.getPrenom_etudiant());
		etudiant5.setPrenom_etudiant("Jeanne");
		assertEquals("Jeanne",etudiant5.getPrenom_etudiant());
		etudiant6.setPrenom_etudiant("Bernadette");
		assertEquals("Bernadette",etudiant6.getPrenom_etudiant());
		etudiant7.setPrenom_etudiant("Alain");
		assertEquals("Alain",etudiant7.getPrenom_etudiant());
	}
	
	@Test
	void testGetLicence() {
		etudiant1.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant1.getLicence());
		etudiant2.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant2.getLicence());
		etudiant3.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant3.getLicence());
		etudiant4.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant4.getLicence());
		etudiant5.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant5.getLicence());
		etudiant6.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant6.getLicence());
		etudiant7.setLicence("BioInformatique");
		assertEquals("BioInformatique",etudiant7.getLicence());
	}

	
	@Test
	void testGetMoyenne() {
		etudiant1.setMoyenne(13.48);
		assertEquals(13.48,etudiant1.getMoyenne(),0.01);
		etudiant2.setMoyenne(11.65);
		assertEquals(11.65,etudiant2.getMoyenne(),0.01);
		etudiant3.setMoyenne(12.54);
		assertEquals(12.54,etudiant3.getMoyenne(),0.01);
		etudiant4.setMoyenne(16.21);
		assertEquals(16.21,etudiant4.getMoyenne(),0.01);
		etudiant5.setMoyenne(17.45);
		assertEquals(17.45,etudiant5.getMoyenne(),0.01);
		etudiant6.setMoyenne(19.58);
		assertEquals(19.58,etudiant6.getMoyenne(),0.01);
		etudiant7.setMoyenne(10.34);
		assertEquals(10.34,etudiant7.getMoyenne(),0.01);
		
	}


	
	@Test
	void testDossierComplet() {
		etudiant1.setDossier_complet(false);
		assertFalse(etudiant1.isDossier_complet());
		etudiant2.setDossier_complet(false);
		assertFalse(etudiant2.isDossier_complet());
		etudiant3.setDossier_complet(false);
		assertFalse(etudiant3.isDossier_complet());
		etudiant4.setDossier_complet(false);
		assertFalse(etudiant4.isDossier_complet());
		etudiant5.setDossier_complet(false);
		assertFalse(etudiant5.isDossier_complet());
		etudiant7.setDossier_complet(false);
		assertFalse(etudiant7.isDossier_complet());
	}
	
	@Test
	void testDossierIncomplet() {
		etudiant6.setDossier_complet(true);
		assertTrue(etudiant6.isDossier_complet());
	}
	

}
